package com.hackathon.shubhackathon;

import static org.testng.Assert.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;
import org.apache.poi.ss.formula.udf.UDFFinder;
import org.json.simple.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class API {
	static RequestSpecification httpRequest;
	
	public static void main(String[] args) {
		System.out.println("***************************** Problem A : 3. id’s for the registered users  ***********************");
		problemA();
		System.out.println("***************************** Problem B***********************");
		problemB();
		System.out.println("***************************** Problem C***********************");
		problemC();
	}
	
	public static void problemA() {
		//problem A
//		System.out.println("***************************** Problem A : i. Get All The Registered Users*********************");
		
		Response prba = getapi("http://35.188.114.237:8088/api/v2/users");
		
//	iv. Assert that the status code for the response received is correct. 	
		checkAPIResponse(prba,"");
		
		String responseBody = prba.getBody().asString();
//		System.out.println("***************************** Problem A : ii. Read all the Id’s for the registered users from the JSON response*********************");
//		System.out.println("Response Body is:" + responseBody);
		String[] arrid = responseBody.split(":");
//		iii. Display the id’s for the registered users on the system console
		for(int i=2;i<arrid.length;i++) {
			System.out.println(arrid[i].split(",")[0].trim());
			i=i+3;
		}
		
		System.out.println("***************************** Problem A done ***********************");
				
	}

	public static void problemB() {
		/*i. Create a request for adding a new user with the following details – 1 mark
		a. Name : <your best friends name>
		b. Last Name: <Your best friends last name>
		c. Address: <Some dummy address or use CThackATAhon>
		ii. Capture the User id generated
		iii. Create a new request for updating the same user with information added/modified for name, lastname and
		address
		iv. Create a new request for deleting the user with id captured in step above.
		v. Make sure to test the response code for all the steps above
		vi. Create a new request for deleting non existing Id, assert the error and ensure that the test passes. It is a negative
		test. 
		*/
		System.out.println("-------------------Add user----------------");
		JSONObject requestParams = new JSONObject();
		requestParams.put("name", "viu"); 
		requestParams.put("surname", "Singh");
		requestParams.put("adress", "simple address cthackthon");
				
		Response prbb = postapi("http://35.188.114.237:8088/api/v2/addusers", requestParams);
		System.out.println("-------------------API Response check----------------");
		checkAPIResponse(prbb,"");
		
		String responseBody = prbb.getBody().asString();
		System.out.println("Response Body is:" + responseBody);
		String uid = responseBody.split(",")[0].split(":")[1].trim();
		System.out.println("Generated user Id is:" + uid);
		requestParams.clear();
		
		System.out.println("-------------------Modify user----------------");
		requestParams.put("id", Integer.parseInt(uid)); 
		requestParams.put("name", "Yuvi"); 
		requestParams.put("surname", "Singh");
		requestParams.put("adress", "complicated address cthackthon");
				
		Response prbb2 = putapi("http://35.188.114.237:8088/api/v2/updateuser", requestParams);
		System.out.println("-------------------API Response check----------------");
		checkAPIResponse(prbb2,"");
		requestParams.clear();
		
		
		System.out.println("-------------------Delete user----------------");
		String deleteurl = "http://35.188.114.237:8088/api/v2/"+uid.toString()+"/deleteuser";
		
		Response prb3del = deleteapi(deleteurl);
		System.out.println("-------------------API Response check----------------");
		checkAPIResponse(prb3del,"");
		
		
		
		System.out.println("-------------------Delete user-negative case----------------");
		//deleting same user which is deleted above
		Response prb3del2 = deleteapi(deleteurl);
		System.out.println("-------------------API Response check----------------");
		checkAPIResponse(prb3del2,"negative");
	}

	public static void problemC() {
	
		/*i. Create a request for adding a new user with the following details – 1 mark
		a. Name : <your family member name>
		b. Last Name: <Your family member last name>
		c. Address: <Some dummy address or use CThackATAhon>
		d. Capture the userid generated
		ii. Visit the following URL https://cpsatexam.org/index.php/challenge-0/
		iii. Now deleting the user with id captured in step above.
		iv. Visit the URL https://cpsatexam.org/index.php/challenge-0/ and confirm that the user
		details are not displayed
		*/
		
		System.out.println("-------------------Add user----------------");
		JSONObject requestParams = new JSONObject();
		requestParams.put("name", "akki"); 
		requestParams.put("surname", "Singh");
		requestParams.put("adress", "simple address cthackthon");
				
		Response prbb = postapi("http://35.188.114.237:8088/api/v2/addusers", requestParams);
		System.out.println("-------------------API Response check----------------");
		checkAPIResponse(prbb,"");
		
		String responseBody = prbb.getBody().asString();
		System.out.println("Response Body is:" + responseBody);
		String uid = responseBody.split(",")[0].split(":")[1].trim();
		System.out.println("Generated user Id is:" + uid);
		requestParams.clear();
		
		System.out.println("-------------------Visit URL---------------");
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\t_shubhada.kulkarni\\Downloads\\chromedriver.exe");  
		ChromeDriver driver = new ChromeDriver();
	    driver.manage().window().maximize();

	    String baseUrl = "https://cpsatexam.org/index.php/challenge-0/";
	    driver.get(baseUrl);
	    
	    driver.close();
	    driver.quit();
	    
	    System.out.println("-------------------Delete user----------------");
		
	    String deleteurl = "http://35.188.114.237:8088/api/v2/"+uid.toString()+"/deleteuser";
		
		Response prb3del = deleteapi(deleteurl);
		System.out.println("-------------------API Response check----------------");
		checkAPIResponse(prb3del,"");
	    
		System.out.println("-------------------Visit URL---------------");
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\t_shubhada.kulkarni\\Downloads\\chromedriver.exe");  
		ChromeDriver driver2 = new ChromeDriver();
	    driver2.manage().window().maximize();
	    driver2.get(baseUrl);
	    
	    List<WebElement> innerText = driver2.findElements(By.xpath("//table/tbody/tr"));
	    System.out.println(innerText.size());
	    String sd = "//table/tbody/tr["+innerText.size()+"]/td[1]";
	    String sd1 = driver2.findElement(By.xpath(sd)).getText();
//	    assertEquals(sd1, uid);
	    
	    driver2.close();
	    driver2.quit();
	    
		
	}
	
	public static Response postapi(String apiurl, JSONObject requestParams) {
		Response response = null;
		try {
			// Specify base URI
			RestAssured.baseURI = apiurl;
//			System.out.println("API url : " + apiurl + "API Req - " + requestParams);
			
			httpRequest = RestAssured.given();
			httpRequest.header("Content-Type", "application/json");

			httpRequest.body(requestParams.toJSONString());

			// Post the request and check the response
			response = httpRequest.post(RestAssured.baseURI);

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return response;

	}

	public static Response putapi(String apiurl, JSONObject requestParams) {
		Response response = null;
		try {
			// Specify base URI
			RestAssured.baseURI = apiurl;
//			System.out.println("API url : " + apiurl + "API Req - " + requestParams);
			
			httpRequest = RestAssured.given();
			httpRequest.header("Content-Type", "application/json");

			httpRequest.body(requestParams.toJSONString());

			// Post the request and check the response
			response = httpRequest.put(RestAssured.baseURI);

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return response;

	}

	
	public static Response getapi(String apiurl) {
		Response response = null;

		try {
			System.out.println("API url : " + apiurl);
			RestAssured.baseURI = apiurl;
			
			httpRequest = RestAssured.given().contentType(ContentType.JSON);
			
			response = httpRequest.get(RestAssured.baseURI);

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return response;
	}

	public static void checkAPIResponse(Response response,String tctype) {
		try {
			int ex_statuscode=200;
			long rsTime = response.timeIn(TimeUnit.MILLISECONDS);
			System.out.println("Response time is : " + rsTime);

			int statusCode = response.getStatusCode();
			System.out.println("Status code is: " + statusCode);
			if(tctype.equalsIgnoreCase("negative")) {
				ex_statuscode=404;
			}
			assertEquals(statusCode, ex_statuscode);
//			if (statusCode == 200 || statusCode == 201) {
//			
//			} else {
//				
//			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	public static Response deleteapi(String apiurl) {
		Response response = null;

		try {
			System.out.println("API url : " + apiurl);
			RestAssured.baseURI = apiurl;
			
			httpRequest = RestAssured.given().contentType(ContentType.JSON);
			
			response = httpRequest.delete(RestAssured.baseURI);

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return response;
	}

}
